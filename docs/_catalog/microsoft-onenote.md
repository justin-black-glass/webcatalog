---
name: Microsoft OneNote
category: Productivity
featured: true
title: Microsoft OneNote for macOS on WebCatalog
key: microsoft-onenote
fullUrl: 'https://www.onenote.com/notebooks'
hostname: onenote.com

---