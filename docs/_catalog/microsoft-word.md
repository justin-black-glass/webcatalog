---
name: Microsoft Word
category: Productivity
featured: true
title: Microsoft Word for macOS on WebCatalog
key: microsoft-word
fullUrl: 'https://office.live.com/start/Word.aspx'
hostname: office.live.com

---