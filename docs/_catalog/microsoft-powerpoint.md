---
name: Microsoft PowerPoint
category: Productivity
featured: true
title: Microsoft PowerPoint for macOS on WebCatalog
key: microsoft-powerpoint
fullUrl: 'https://office.live.com/start/PowerPoint.aspx'
hostname: office.live.com

---